/* eslint-env node */

module.exports = function (config) {
    config.set({

        basePath: '',

        frameworks: ['mocha'],

        files: [
            'src/js/**/*.test.js',
            'src/test-fixture/**/*.html'
        ],

        exclude: [],

        // Note:
        // - Webpack is always needed to handle modules (import/export)
        // - Babel Loader is only needed for funning test in old browsers (i.e. IE, PhantomJS ...)
        preprocessors: {
            'src/js/**/*.js': ['webpack', 'sourcemap'],
            'src/test-fixture/**/*.html': ['html2js']
        },

        webpack: {
            mode: 'development',
            module: {
                rules: [
                    {
                        test: /\.js$/,
                        use: 'babel-loader',
                        exclude: /node_modules/
                    }
                ]
            }
        },

        webpackMiddleware: {
            noInfo: true,
            stats: { chunks: false }
        },

        reporters: ['progress'],
        // reporters: ['progress', 'growl'],

        port: 9876,

        colors: true,

        logLevel: config.LOG_INFO,
        failOnEmptyTestSuite:false,

        autoWatch: true,

        // browsers: ['PhantomJS'],
        browsers: ['ChromeHeadless'],
        // browsers: ['Chrome'],
        // browsers: ['Safari'],
        // browsers: ['Chrome', 'Firefox', 'Safari'],

        singleRun: true,

        concurrency: Infinity
    });
};
