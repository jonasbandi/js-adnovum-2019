/* eslint-env node */

var env = process.env.BABEL_ENV;

const presets = [
    ['@babel/env', {
        targets: {
            edge: '17',
            firefox: '60',
            chrome: '67',
            safari: '11.1'
        },
        'modules': env === 'test' ? 'commonjs' : false
    }]
];

const plugins = [
  '@babel/plugin-proposal-class-properties'
];

module.exports = { presets, plugins };
