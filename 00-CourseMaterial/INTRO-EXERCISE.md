## Exercise 01: Three Apps

Compare the three ToDo-applications at the following urls:

- [https://jba-todo.now.sh/todo1/](https://jba-todo.now.sh/todo1/)
- [https://jba-todo.now.sh/todo2/](https://jba-todo.now.sh/todo2/)
- [https://jba-todo.now.sh/todo3/](https://jba-todo.now.sh/todo3/)

What can you find out about their technical implementation?  
Write down all the differences.

(The source code for the three examples is in `01-Intro/01-ToDo-Apps`)
